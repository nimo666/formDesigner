/**
 * 文本
 */
export let text = {
    id:'',
    _id:'',
    compType: 'text',
    ele: 'fancy-text',
    compName:'文本',
    compIcon:'text',
    //展示表单的模式
    viewType:'component',
    config: true,
    form:false,
    show:true,
    text:'文本',
    // 控件左侧label内容
    showLabel:false,
    labelWidth: '0'
  }
  