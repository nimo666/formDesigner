## 简介 
基于vue+ElementUI的表单设计器。让表单拖拽更加简单方便
![Image text](https://gitee.com/wurong19870715/formDesigner/raw/master/public/img/designer.png)
![Image text](https://gitee.com/wurong19870715/formDesigner/raw/master/public/img/preview.png)
![Image text](https://gitee.com/wurong19870715/formDesigner/raw/master/public/img/edit-form.png)
![Image text](https://gitee.com/wurong19870715/formDesigner/raw/master/public/img/config-json.png)
![Image text](https://gitee.com/wurong19870715/formDesigner/raw/master/public/img/val-json.png)
![Image text](https://gitee.com/wurong19870715/formDesigner/raw/master/public/img/dialogList.png)


#开发日志
- 2020-12-15: 增加button组件
- 2020-12-25: 增加分割线组件
- 2020-12-28: 增加Alert 警告组件
- 2021-01-01: 增加文本组件
- 2021-01-01: 增加html组件
- 2021-01-01: 增加编辑器
- 2021-01-07: 增加json查看
- 2021-01-09: 增加icon选择组件，增加button选择按钮的支持
- 2021-01-12: 增加编辑器字数限制的校验、增加input输入框前后图标的配置
- 2021-01-18: 增加form-builder组件，增加渲染，查看配置统一界面，查看渲染界面
- 2021-02-01: 增加颜色选择器
- 2021-02-08: 增加级联选择器（包含省市区联动）
- 2021-02-14: 增加附件组件
- 2021-03-23: 增加dialogList,支持更多信息展示的列表

## 后期规划
- 常用组件基本开发完毕，后续需要对各个组件、代码慢慢打磨。
- 增加自定义验证功能
- 增加后端的示例。
- 新分支开发新组件

## 今年要写论文，业余时间可能不会那么快更新这个项目，抱歉！

## 交流一下

QQ群：816204482

## 预览地址
  [formdesigner](http://wurong19870715.gitee.io/formdesigner)
  
## 感谢作者
  [form-generator](https://gitee.com/mrhj/form-generator)

如果这个项目对您有帮助，麻烦给个star，谢谢！

## 开源协议
[MIT]
